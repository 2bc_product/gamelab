
import pygame, sys
from pygame.locals import *
from util.vec2d import Vec2d

from random import randrange, random
import math

import g

class MovingObject(object):
  def __init__(self):
    self.pos = Vec2d(randrange(0, g.WIDTH), randrange(0, g.HEIGHT))
    self.vel = Vec2d(randrange(-50, 50), randrange(-50, 50))
    self.acc = Vec2d(0, 0)
    self.angle = 0

  def update(self, dt):
    self.vel += dt * self.acc

    self.pos += dt * self.vel
    self.pos.x = self.pos.x % g.WIDTH
    self.pos.y = self.pos.y % g.HEIGHT

    self.angle = math.atan2(self.vel.y, self.vel.x) * 180/math.pi

class Ship(MovingObject):
  radius = 20

  def __init__(self):
    MovingObject.__init__(self)

    self.pos = Vec2d(randrange(0, g.WIDTH), randrange(0, g.HEIGHT))
    self.vel = Vec2d(randrange(-50, 50), randrange(-50, 50))
    self.acc = Vec2d(0, 0)

    self.color = (randrange(64,200), randrange(64,200), randrange(64,200))

    self.score = 0

    self.alive = True
    
  def update(self, dt):
    MovingObject.update(self, dt)

    # compute the three points of the ship

    self.points = [Vec2d(self.radius*math.cos(math.radians(th)), self.radius*math.sin(math.radians(th))) for th in [0, 180-30, 180+30]]
    self.points = [p.rotated(self.angle) for p in self.points]
    self.points = [p + self.pos for p in self.points]

  def draw(self):
    pygame.draw.polygon(g.window, self.color, self.points)
    pygame.draw.circle(g.window, self.color, map(int, self.pos), self.radius, 1)


class Bullet(MovingObject):
  radius = 3
  color = (255, 255, 255)

  def __init__(self, ship):
    MovingObject.__init__(self)

    self.ship = ship
    self.ttl = 1.0
    self.vel = Vec2d(ship.vel) + 200*ship.vel.normalized()
    self.pos = Vec2d(ship.pos)

  def update(self, dt):
    MovingObject.update(self, dt)
    self.ttl -= dt

  def draw(self):
    pygame.draw.circle(g.window, self.color, map(int, self.pos), self.radius, 0)

class Obstacle(MovingObject):
  color = (255, 0, 0)

  def __init__(self):
    MovingObject.__init__(self)
    self.pos = Vec2d(randrange(0, g.WIDTH), randrange(0, g.HEIGHT))
    self.radius = randrange(20, 75)

  def update(self, dt):
    MovingObject.update(self, dt)

  def draw(self):
    pygame.draw.circle(g.window, self.color, map(int, self.pos), self.radius, 1)

class Gem(object):
  radius = 16
  color = (0, 0, 255)

  def __init__(self):
    self.pos = Vec2d(randrange(0, g.WIDTH), randrange(0, g.HEIGHT))
    self.visible = True
    self.image = pygame.image.load(g.assetpath + "/smiley.png")

  def update(self, dt):
    if not self.visible and random() < 0.01:
      self.__init__()

  def draw(self):
    if self.visible:
      #pygame.draw.circle(g.window, self.color, map(int, self.pos), self.radius, 0)
      g.window.blit(self.image, map(int, self.pos-16))

class Game(object):
  def __init__(self):
    pass

  def setup(self):
    self.ships = []
    self.obstacles = []
    self.gems = []
    self.bullets = []

    for i in range(5):
      self.obstacles.append(Obstacle())

    for i in range(5):
      self.gems.append(Gem())

    self.dth = 0
    self.th = 0

  def update(self, dt):
    for ship in self.ships:
      ship.vel = 128*ship.player.tilt.normalized()
      ship.update(dt)

      for obstacle in self.obstacles:
        if ship.pos.get_distance(obstacle.pos) < ship.radius + obstacle.radius:
          ship.alive = False

      # check if I intersect with any gems
      for gem in self.gems:
        if gem.visible and ship.pos.get_distance(gem.pos) < gem.radius + ship.radius:
          ship.score += 10
          gem.visible = False

      # check if the ship intersects with any bullets
      for bullet in self.bullets:
        if bullet.ship != ship and ship.pos.get_distance(bullet.pos) < ship.radius + bullet.radius:
          ship.alive = False

      if ship.alive:
        ship.player.set_value("%d" % ship.score)
      else:
        ship.player.set_value("game over (%d)" % ship.score)
        ship.player.disconnect()

    self.ships = [ship for ship in self.ships if ship.alive]
    self.bullets = [bullet for bullet in self.bullets if bullet.ttl > 0]

    for bullet in self.bullets: bullet.update(dt)

    for obstacle in self.obstacles: obstacle.update(dt)
    for gem in self.gems: gem.update(dt)


  def draw(self):
    g.window.fill((0,0,0))

    for ship in self.ships: ship.draw()
    for bullet in self.bullets: bullet.draw()

    for gem in self.gems: gem.draw()
    for obstacle in self.obstacles: obstacle.draw()

  def keydown(self, key):
    if key == 276: self.dth = -5
    if key == 275: self.dth =  5
    if key == 32:
      self.bullets.append(Bullet(self.ships[0]))

  def keyup(self, key):
    self.dth = 0

  def button(self, player_id):
    for ship in self.ships:
      if ship.player_id == player_id:
        self.bullets.append(Bullet(ship))

  def connect(self, player_id):
    print "%s connected" % player_id
    ship = Ship()
    ship.player_id = player_id
    ship.player = g.players[player_id]
    self.ships.append(ship)
    ship.player.set_color(ship.color)

  def disconnect(self, player_id):
    print "%s disconnected" % player_id
    self.ships = [ship for ship in self.ships if ship.player_id != player_id]
