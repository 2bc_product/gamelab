import pygame, sys
from pygame.locals import *
from util.vec2d import Vec2d

from random import randrange, random
import math

import g

from game import Game

def setup():
  # set up pygame
  pygame.init()

  size = (pygame.display.Info().current_w, pygame.display.Info().current_h)
  g.WIDTH, g.HEIGHT = size
  print "Framebuffer size: %d x %d" % (size[0], size[1])
  g.window = pygame.display.set_mode(size, pygame.FULLSCREEN)
  pygame.display.set_caption('gamelab')

  g.game.setup()

  # run the game loop
  g.clock = pygame.time.Clock()

def once(msgs=[]):
  dt = g.clock.tick() / 1000.0

  for event in pygame.event.get():
    if event.type == QUIT:
      pygame.quit()
      sys.exit()
    if event.type == KEYDOWN:
      if event.key == 27:
        pygame.quit()
      g.game.keydown(event.key)
    if event.type == KEYUP:
      g.game.keyup(event.key)

  for msg in msgs:
    if False: pass
    elif msg["event"] == "button":
      g.game.button(msg["player_id"])
    elif msg["event"] == "connect":
      g.game.connect(msg["player_id"])
    elif msg["event"] == "disconnect":
      g.game.disconnect(msg["player_id"])

  g.game.update(dt)
  g.game.draw()

  pygame.display.update()

