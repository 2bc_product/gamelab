from gevent import monkey; monkey.patch_all()

from socketio import socketio_manage
from socketio.server import SocketIOServer
from socketio.namespace import BaseNamespace
from socketio.mixins import RoomsMixin, BroadcastMixin

import gevent.queue
import random
import string

import math
from util.vec2d import Vec2d

import gevent
import sys
import traceback

import g

msg_queue_in = gevent.queue.Queue()

class GameNamespace(BaseNamespace):
  def __init__(self, *args, **kwargs):
    BaseNamespace.__init__(self, *args, **kwargs)

    self.id = ''.join(random.choice(string.lowercase) for x in range(8))
    self.tilt = Vec2d(0,0)
    self.initialized = False

  def on_hello(self, msg):
    g.players[self.id] = self
    msg_queue_in.put({
      'player_id': self.id,
      'event': "connect"
    })
    self.initialized = True

  def on_button(self):
    if not self.initialized: return
    msg = {
      'player_id': self.id,
      'event': "button",
    }
    msg_queue_in.put(msg)

  def on_orientation(self, bg):
    if not self.initialized: return
    beta, gamma = bg
    self.tilt = Vec2d(math.sin(math.radians(gamma)), math.sin(math.radians(beta)))

  def recv_disconnect(self):
    if not self.initialized: return
    msg_queue_in.put({
      'player_id': self.id,
      'event': "disconnect"
    })
    try:
      del g.players[self.id]
    except KeyError:
      pass
    self.disconnect(silent=True)

  def set_value(self, value):
    self.emit('value', value)

  def set_color(self, color):
    self.emit('color', color)

class Application(object):
  def __init__(self):
    self.buffer = []
    g.players = {}

  def __call__(self, environ, start_response):
    path = environ['PATH_INFO'].strip('/')

    if not path: path = "static/index.html"

    if path.startswith('static/'):
      try:
        data = open("gamelab/"+path).read()
      except Exception:
        return not_found(start_response)

      if path.endswith(".js"):
        content_type = "text/javascript"
      elif path.endswith(".css"):
        content_type = "text/css"
      else:
        content_type = "text/html"

      start_response('200 OK', [('Content-Type', content_type)])
      return [data]
    elif path.startswith("socket.io"):
      socketio_manage(environ, {'': GameNamespace}, {})
    elif path.startswith("branch/"):
      branch = path.lstrip("branch/")

    else:
      return not_found(start_response)

def not_found(start_response):
  start_response('404 Not Found', [])
  return ['<h1>Not Found</h1>']


def gpgmain():
  while True:
    msgs = []
    while True:
      try:
        msg = msg_queue_in.get(block=False)
        msgs.append(msg)
      except gevent.queue.Empty:
        break
    try:
      pgmain.once(msgs)
    except Exception, e:
      traceback.print_exc(e)
      sys.exit()

    gevent.sleep(0.001)

import pgmain
import imp
def main(name):
  # load the game
  g.path = "gamelab/games/%s" % name
  g.assetpath = "%s/assets/" % g.path
  module = imp.load_source("game", "%s/game.py" % g.path)
  g.game = module.Game()

  pgmain.setup()

  gevent.spawn(gpgmain)
  print 'Listening on port 80 and on port 843 (flash policy server)'
  SocketIOServer(('0.0.0.0', 80), Application(),
    resource="socket.io", policy_server=True,
    policy_listener=('0.0.0.0', 10843)).serve_forever()

if __name__ == '__main__':
  main()
