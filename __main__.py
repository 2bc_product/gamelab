import server

import sys


try:
  name = sys.argv[1]
except IndexError:
  name = "smashtroids"

server.main(name)
